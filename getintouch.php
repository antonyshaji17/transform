        <!-- GET IN TOUCH START  -->
        <div class="row pt-5 pb-3" id="contact">
            <div class="col-md-12">
            <h2>
                <span class="featured text-center">
                    <span>GET IN TOUCH</span>
                </span>
            </h2>
            </div>
        </div>
        <div class="row m-0">
            <div class="col-md-6">
                <p style=" width: 90%; ">Transform Group commits to be the best, to inspire, incubate and impact the environments and experiences of our premium customers, building projects.</p>
                <ul class="list-unstyled mb-0" style=" width: 90%; ">
                    <li><i class="fa fa-volume-control-phone fa-fw" aria-hidden="true"></i>+91 44 2434 2734
                    </li>
                    <li><i class="fa fa-envelope fa-fw" aria-hidden="true"></i>info@transformdesign.net
                    </li>
                    <li><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>10, ARK Colony Rd, Vannia Teynampet, Lubdhi Colony, Alwarpet, <br><span style=" margin-left: 4%; ">&nbsp</span> Chennai, Tamil Nadu 600018
                    </li>
                </ul>
            </div>
            <div class="col-md-6 pt-3 pb-3 mb-3 border border secondary">
                <form method="POST" id="myForm" action="sendmail.php">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputName1">Name*</label>
                                <input type="Name" name="sender_nam‌​e​" class="form-control" id="exampleInputName1" aria-describedby="NameHelp" placeholder="Enter Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email*</label>
                                <input type="email" name="sender_email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mobile*</label>
                                <input type="Mobile" class="form-control" id="exampleInputMobile1" aria-describedby="MobileHelp" placeholder="Enter Mobile" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputMessage1">Message*</label>
                                <textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="3" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="text-center text-md-center">
                        <button type="submit" class="btn btn-danger">Submit Now</button>
                    </div>
                </form>
                <div class="text-center">
                <p class="formSentMsg" style="display:none;color:green;">Thank you! Your mail has been sent successfully.</p>
                </div>
            </div>
        </div>
        <!-- GOOGLE MAPS START  -->
        <div class="row">
            <div class="col-md-12">
                <div class="map-responsive">
                    <div class="mapouter">
                        <div class="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.969521550152!2d80.25238911367806!3d13.03761201695662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5266357b9a1343%3A0xf146d790f64ed9e0!2sTransform%20Design!5e0!3m2!1sen!2sus!4v1632016838368!5m2!1sen!2sus"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen></iframe>
                            <a href="https://putlocker-is.org"></a><br>
                        </div>
                    </div>

                    <!-- <iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=Eiffel+Tower+Paris+France" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                </div>
            </div>
        </div>
        <!-- GOOGLE MAPS END  -->