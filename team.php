<?php
$request = $_SERVER['REQUEST_URI'];
if (($request == "/team.php") || ($request == "/tdesign/team.php")) {
    include('header-main.php');
}
?>
<!-- <div class="row mt-5">
&nbsp;
</div> -->
<!-- OUR TEAM START  -->
<div class="row pt-3" id="our-team">
    <div class="col-md-12">
        <h2>
            <span class="featured text-center">
                <span>MEET OUR TEAM</span>
            </span>
        </h2>
    </div>
</div>
<div class="row teamone">
    <div class="col-md-4">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/KS.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Krithika Subrahmanian</p>
                    <p class="profiletitle">Founder|Managin Director</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#exampleModalLong"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Krithika Subrahmanian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Founder I Managing Director | Transform Design Pvt. Ltd.</p>
                        <p class="teamdesignation">Co-Founder | SRESHTA LEISURE Pvt. Ltd.,</p>
                        <p>Heads dynamic group of companies in the fields of architecture, design, construction and luxury hospitality. Her expertise in planning, designing, conservation and construction is unparalleled. With a keen acumen for developing business models that deliver
                            on growth and expansion goals, Krithika has completed around 1600 projects and is a deeply rooted third generation entrepreneur and culture expert. She was awarded Best Luxury Design provider and was the winner of the TATLER
                            Travel Award 2020 for SVATMA, her hospitality venture. Krithika Subrahmanian loves all things sustainable and is constantly looking for solutions to create a sustainable future for her group of companies by staying on the
                            cutting-edge of emerging technology and new methodologies. She is a successful entrepreneur in conceiving and delivering space functionality. With 25-year old strong businesses in her asst list, her new ventures are attractive
                            and scalable investment propositions. She is a successful entrepreneur in conceiving and delivering space functionality. With 25-year old strong businesses in her asset list, her new ventures are attractive and scalable
                            investment propositions.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/SRIMAN.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Sriman Subramanian</p>
                    <p class="profiletitle">Promoter</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile2"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile2" tabindex="-1" role="dialog" aria-labelledby="profile2 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile2">Sriman Subramanian</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Promoter</p>
                        Sriman is focused on Design Management and developmental work in Architectural projects and products, international marketing and trade, developing capabilities across the globe in high quality luxury products and unique projects of sustainable, environmentally
                        sensitive and adaptive value. Proficient in French, Italian, Farsi and Arabic.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/vivek.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Vivek Anandhan. N.V</p>
                    <p class="profiletitle">Chief executive officer</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile5"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile5" tabindex="-1" role="dialog" aria-labelledby="profile5 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile5">Vivek Anandhan. N.V</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Chief executive officer</p>
                        <p>Architect –Planner with over 20 years’ experience in planning, designing, detailing and coordinating with multi disciplinary teams and execution of projects.</p>
                        <p>Employee of the Year. Mouchel 2007, 2008</p>
                        <p>Won ISHA building design, Bangalore, India</p>
                        <p><b>HEAD DESIGN, INDIA & GCC</b></p>
                        <p>Heading the design team of Architects and liaising with Engineers of various disciplines. Responsible for spearheading all projects on the Design front including investments, joint ventures, management Contract projects. Advise development</p>
                        <p>Responsible for all aspects of project programming, architectural design, mechanical and electrical drawing coordination, construction documents and construction supervision Was also responsible for preparation of construction documents and specifications for the projects. Develop presentation drawings and coordinate drawings with other disciplines. Applied artistic expertise and had the opportunity to work on a variety of projects including educational, municipal, professional office, institutional, commercial, religious, recreational, and residential. Responsible for site design, consultant coordination, in house coordination with the design team etc.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row teamtwo">
    <div class="col-md-3">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/Profile 2.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Mr. Shriram</p>
                    <p class="profiletitle">Operational Leadership</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile5"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile5" tabindex="-1" role="dialog" aria-labelledby="profile5 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile5">Mr. Shriram</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Operational Leadership</p>
                        <p>K. Shriram, a practicing Chartered Accountant, is an expert in the field of Business Advisory Services, Financial Analysis and Assurance & Compliance.</p>
                        <p>Has started his career in GCC region as a Group Corporate Assurance Executive and then continued in CRISIL as an Equity Research Analyst.</p>
                        <p>He has been working as a Finance Head in an MNC. On discovering his passion in the field of Financial Advisory, he started his own practice and since then has been working with various Managements offering solutions
                            on Business & Strategic management.</p>
                        <p>His area of specializing includes CFO outsourcing services and he offers a bouquet of services including Business Process Management, Strategic Analysis, Business Life Cycle diagnostics and advisory services. Shriram
                            is presently a Director in a Chennai based Consulting Firm specializing in Business Consultancy Services.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/Karthik Profile.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Mr. Karthik Ramani</p>
                    <p class="profiletitle">Operational Leadership</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile6"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile6" tabindex="-1" role="dialog" aria-labelledby="profile6 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile6 Title">Mr. Karthik Ramani</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Operational Leadership</p>
                        <p>Headed teams rolling out brands like KFC, Taco Bell, Star bucks, Spencer retail in India. Retail sotres for Apparel Landmark, Homes ‘r’ Us in Dubai, Toys ‘r’ Us, Cold stone creamery in Springfield, USA and Women’s secret
                            in Europe. Accomplished executive with substantial experience in leading all phases of construction/design project management across retail and hospitality sector. Track record of success in design/management of design
                            challenges, sketching, project budgeting, AutoCad, vendor management, electro mechanical services and crisis management.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/Sriram Profile.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename" style="">Mr. Sriram V. Rajagopal</p>
                    <p class="profiletitle">Management Advisory</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile3"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile3" tabindex="-1" role="dialog" aria-labelledby="profile3 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile3 Title">Mr. Sriram V. Rajagopal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Management advisory</p>
                        <p>Acutely conscious of the escalating costs in maintaining a ‘bench’ and the gap in supply-demand for the right talent, Sriram has envisioned an ecosystem that would equally empower candidates and companies to find the right
                            fit, irrespective of where the opportunity existed. </p>
                        <p>Has 18 years of experience in the global talent supply chain in Cognizant. For Sriram Rajagopal, Diamond Pick is his dream of talent democratization come true.
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card__collection clear-fix">
            <div class="cards cards--three">
                <img src="./profiles/chakravarthi Profile.jpg" class="img-responsive" alt="">
                <span class="cards--three__rect-1 text-right">
                    <span class="shadow-1"></span>
                    <p class="profilename">Mr. Chakravarthi</p>
                    <p class="profiletitle">Management Advisory</p><a class="profilereadmore" href="#" data-toggle="modal" data-target="#profile4"> Read More </a></p>
                </span>
                <span class="cards--three__rect-2">
                    <span class="shadow-2"></span>
                </span>
                <ul class="cards--three__list">
                    <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                </ul>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="profile4" tabindex="-1" role="dialog" aria-labelledby="profile4 Title" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="profile4 Title">Mr. Chakravarthi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="teamdesignation">Management Advisory</p>
                        <p>Highly experienced in Construction and Project Management. As Chief Projects Officer at Apollo Hospital Enterprises Limited, he was responsible for the leadership for the complete management of new hospital projects (brown
                            field as well as green field and renovations) from finalization of design, contract management, monitoring of execution and obtaining statutory approvals to commissioning.
                        </p>
                        <p>He has 34 years of experience in the Indian Army in the field of Infrastructure development, from planning to execution and managing vast technical resources.
                        </p>
                        <p>Mr. Chakravarthi had the opportunity of handling leadership positions in the fields of Highways, Airfields and Buildings (large scale living and technical accommodation for army units, Industrial buildings for production
                            of multifarious kinds of equipment, vehicles, ammunition, missiles, Infrastructure for educational establishments, sports stadiums including swimming pools, married accommodation complexes, radial well in a river and
                            large scale specialized explosive store houses.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- OUR TEAM END  -->
<?php
if (($request == "/team.php") || ($request == "/tdesign/team.php")) {
    include('footer-main.php');
}
?>