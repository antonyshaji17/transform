<?php
$request = $_SERVER['REQUEST_URI'];
if (($request == "/articles-carousel.php") || ($request == "/tdesign/articles-carousel.php")) {
    include('header-main.php');
}
?>
<!-- ARTICLES SLIDER START  -->
<div class="row pt-5 pb-3" id="articles">
    <div class="col-md-6">
        <h2>
            <span class="featured text-center">
                <span>ARTICLES</span>
            </span>
        </h2>
    </div>
    <div class="col-md-6 text-right">
        <span class="float:right"><a class="btn btn-danger" href="/articles.php">View all articles</a></span>
    </div>
</div>
<div class="row">
    <div class="owl-carousel owl-theme">
        <?php
        for ($i = 1; $i < 14; $i++) {
            echo '<div class="item" style="width:250px"><a class="item popup" href="./images/articles/' . $i . '.jpg"><img src="./articles/' . $i . '.jpg" alt="Bootleggers Ball"></a>  <p class="img__description"><a class="item popup btn btn-danger" href="./images/articles/' . $i . '.jpg">Read More</a></p>
    </div>';
        }
        ?>
    </div>
</div>
<?php
$request = $_SERVER['REQUEST_URI'];
if (($request == "/articles-carousel.php") || ($request == "/tdesign/articles-carousel.php")) {
    include('footer-main.php');
}
?>