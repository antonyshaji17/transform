<?php
$request = $_SERVER['REQUEST_URI'];
if (($request == "/articles.php") || ($request == "/transform/articles.php")) {
    include('header-main.php');
}
?>

<!-- ARTICLES SLIDER START  -->
<div class="row pt-5 pb-3 mt-3" id="articles" style=" background-color: #dedcdc; ">
    <div class="col-md-12 mt-3 mb-3 text-center">
        <h3><u>ARTICLES</u></h3>
    </div>
</div>
<div class="row text-center" style=" background-color: #dedcdc; ">

    <?php
    for ($i = 1; $i < 89; $i++) {
        echo '<div class="col-md-3 mb-4"><a class="popup" href="./images/articles/' . $i . '.jpg"><img src="./images/articles/' . $i . '.jpg" alt="Bootleggers Ball" style="width:250px;height:300px"></a></div>';
    }
    ?>

</div>

<!-- ARTICLES SLIDER END  -->

<?php
$request = $_SERVER['REQUEST_URI'];
if (($request == "/articles.php") || ($request == "/tdesign/articles.php")) {
    include('footer-main.php');
}
?>