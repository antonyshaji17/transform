        <!-- FOOTER START  -->
        <div class="row justify-content-md-center pt-3 pb-3" style="" id="main-footer">
            <div class="col-md-4">
                <a href="/"><img src="./logos/transformlogo.png" alt="" class="wp-image-6" width="200" height="45" /></a>
                <p class="mt-2 footerlogotext">Transform Group commits to be the best, to inspire, incubate and impact the environments and experiences of our premium customers, building projects
                </p>
            </div>
            <div class="col-md-4 footerfaicons">
                <p class="h3">CONTACT US</p>
                <ul class="list-unstyled mb-0 pt-2 footeraddress">
                    <li><i class="fa fa-volume-control-phone fa-fw" aria-hidden="true"></i>+91 44 2434 2734
                    </li>
                    <li><i class="fa fa-envelope fa-fw" aria-hidden="true" style=" font-size: 16px; "></i>info@transformdesign.net
                    </li>
                    <li><i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>10, ARK Colony Rd, Vannia Teynampet,<br><span style=" margin-left: 9%; "></span>   Lubdhi Colony, Alwarpet, Chennai,<br><span style=" margin-left: 9%; "></span>Tamil Nadu 600018</li>
                </ul>
            </div>
            <div class="col-md-4 text-left">
                <p class="h3 footerlinks">INFORMATION</p>
                <ul class="pt-3 footerlinks" style="list-style: none;">
                    <li><i class="icon-arrow-right"></i><a href="/"><i class="fa fa-angle-right" aria-hidden="true"></i>Home</a></li>
                    <li><a href="#logocards"><i class="fa fa-angle-right" aria-hidden="true"></i>Group of Companies</a></li>
                    <li><a href="#our-team"><i class="fa fa-angle-right" aria-hidden="true"></i>Leadership</a></li>
                    <li><a href="#articles"><i class="fa fa-angle-right" aria-hidden="true"></i>Articles</a></li>
                    <li><a href="#contact"><i class="fa fa-angle-right" aria-hidden="true"></i>Contact</a></li>
                </ul>
            </div>
        </div>
        <div class="text-center p-2" style="background-color:white;"> © 2021 Copyright: <a class="text-reset fw-bold" href="https://transform.bestsmmgoogle.com/">Transform Group</a> </div>
        <!-- FOOTER END  -->
        </div>


        <script>
            $(document).ready(function() {
                $('.counter').each(function() {
                    $(this).prop('Counter', 0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 4000,
                        easing: 'swing',
                        step: function(now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                $('.owl-carousel').owlCarousel({
                    // loop: true,
                    // autoplay:true,
                    margin: 10,
                    // responsiveClass: true,
                    // nav: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                })
            });
        </script>

        <script>
            $(document).ready(function() {
                $("#myForm").on('submit', function(event) {
                    event.preventDefault();
                    var formData = $(this).serialize();
                    $.ajax({
                        type: 'POST',
                        url: 'sendmail.php',
                        dataType: "json",
                        data: formData,
                        success: function(response) {
                            if (response != "sent") {
                                $(".formSentMsg").css('display', 'block')
                                $(".formSentMsg").delay(3200).fadeOut(300);
                            }
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr);
                        }
                    });
                });
            });
        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>



        <script>
            $(document).ready(function() {
                $(".popup").magnificPopup({
                    type: "image",
                    removalDelay: 160,
                    preloader: false,
                    fixedContentPos: true,
                    gallery: {
                        enabled: true
                    }
                });
            });
            $(".owl-carousel").owlCarousel({
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                loop: true,
                margin: 10,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                },
                nav: true,
                navText: [
                    "<i class='fa fa-angle-left'></i>",
                    "<i class='fa fa-angle-right'></i>"
                ]
            });
        </script>

        </body>

        </html>