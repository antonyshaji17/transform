<?php 
    include('header-main.php');
?>
        <div id="carouselExampleIndicators" class="carousel slide mt-5" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="./images/cover/cover1.jpg" height="500" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="./images/cover/cover2.jpg" height="500" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="./images/cover/cover3.jpg" height="500" alt="Third slide">
                </div>
            </div>
            <!--  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a> -->
        </div>

        <!-- COUNTERS START  -->
        <div class="row" id="counters-block">
            <div class="four col-md-3">
                <div class="counter-box">
                    <span class="counter">25</span>
                    <p>Years of project experience</p>
                </div>
            </div>
            <div class="four col-md-3">
                <div class="counter-box">
                    <span class="counter">1600</span>
                    <p>Projects Done</p>
                </div>
            </div>
            <div class="four col-md-3">
                <div class="counter-box">
                    <span class="counter">100</span>
                    <p>Architects and Engineers</p>
                </div>
            </div>
            <div class="four col-md-3">
                <div class="counter-box">
                    <span class="counter">4000</span>
                    <p>Happy Experinces</p>
                </div>
            </div>
        </div>
        <!-- COUNTERS END  -->
        <!-- logos flips starts  -->

        <div class="row no-gutters justify-content-center pt-5" id="logocards">
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0 border-left-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo logo-one" src="./images/logos/tdlogo.png" alt="Avatar" style=" width: 300px; height: 65px; left: 12%;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>Transform Design</h3>
                            <p>We are a multidisciplinary practice offering Architecture, Master planning, Construction, Project Management services and Interior Design. We believe that all problems can be solved with good design.</p>
                            <a href="http://demo.transformdesign.net/" target="_blank">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/copyofcode.png" alt="Avatar" style="width:150px;height:50px;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>CODE</h3>
                            <p>CODE is delivering its projects in terms of design-build, right from legal, design co-ordination, financial estimates, procurement, civil construction and interior execution till handing over the project to its clients aiming to meet the client’s requirement in order to produce a functionally and financially viable project.</p>
                            <a href="http://code.bestsmmgoogle.com/">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0 border-right-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/decode.png" alt="Avatar" style="width:150px;height:50px;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>DECODE</h3>
                            <p>DECODE designs in a way that promotes an enjoyable and hassle- free shopping experience for the ultimate consumer. DECODE is a “one stop” services supply chain that includes design, execution, products and branding.</p>
                            <a href="#">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters justify-content-center" id="logocards">
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0 border-left-0 border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/metallp.png" alt="Avatar" style="width: 275px; height: 50px; left: 16%;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>METAFORE DESIGN LLP</h3>
                            <p>Metafore Design LLP is a dynamic India based consortium which provides technology oriented project advisory and consultancy services. It was founded by two renowned leading architects as promoters in 2019. Krithika Subrahmanian (Transform Group) and Cheralathan (Cheralathan associates)</p>
                            <a href="http://metaforedesignllp.com/" target="_blank">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0 border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/cdclogo.png" alt="Avatar" style="width:150px;height:50px;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>CDC PORJECTS INFRAA LLP</h3>
                            <p>CDC Projects Infraa LLP is an India-based organization established by reputed entrepreneurs who have achieved excellent exposure and wide experience in the field of construction.</p>
                            <a href="http://cdcprojectsinfraa.com/" target="_blank">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card border-top-0 border-right-0 border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/metafore.png" alt="Avatar" style="width: 150px; height: 31px; top: 45%;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>METAFORE</h3>
                            <p>We have extensive and detailed approach to curating and building brands, luxury with a conscience and sensitivity to conserving identity of the customer, of the product and experience.</p>
                            <a href="http://metaforedesign.com/">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters justify-content-center" id="logocards">
            <div class="col-md-4 text-center allborder">
                <div class="flip-card  border-left-0 border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo saa-image" src="./logos/saa.png" alt="Avatar" >
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>SAA Wellness</h3>
                            <p>Saa is for the Organic Self Care Naturalist. Break free of indoctrination for we broke free of all jargon . Saa Ritual is simple , personable and totally focussed on the intuitive . Six blends that add fragrance and flavour to skin care. Pure wellness on first principles .The Origins of which are precious, authentic and blended to perfection to be understated. Aromatherapy and Naturopathy anchored by a tempered Sidha practice. Prompted by impulses that elicit a response, elevate the experience of self care with high quality true origin.</p>
                            <a href="#">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card  border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/svatma.png" alt="Avatar" style="width: 123px; height: 171px; top: 10%; left: 36%;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>SVATMA</h3>
                            <p>SVATMA - SVA (one's own) ATMA(soul) - literally translates as ONE'S OWN SOUL. the very personal journey of an individual seeking the true artistry of the spirit within.</p>
                            <a href="https://www.svatma.in/">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center allborder">
                <div class="flip-card  border-right-0 border-bottom-0">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <img class="card-logo" src="./logos/synck.jpg" alt="Avatar" style="width: 150px; height: 186px; top: 10%;">
                        </div>
                        <div class="flip-card-back align-middle pt-3">
                            <h3>SYNCK</h3>
                            <p>SYNCK opens as Chennai’s largest culinary destination that celebrates inclusivity, progressive naturalism and fresh Glocal cuisine. An enjoyable rich diversity of flavours and lifestyle experiences curated in the distinctive ambiance of an urban retreat. Located at the vibrant beachfront of Bessie/ Besant Nagar, SYNCK is a daily destination to lift the spirits in work and life.</p>
                            <a href="https://website.synck.in/">Learn More</a>
                            <ul class="card-back">
                                <li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- logo flips end  -->

<?php 

include('articles-carousel.php'); 
include('team.php'); 
include('getintouch.php'); 
include('footer-main.php');

    ?>
